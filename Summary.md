[S3 Mirror Lite](https://bitbucket.org/plantheric/s3-mirror-lite) is a tool for Windows that allows you to mirror local directories to Amazon S3.  
S3 Mirror can be run as either a command line application or installed as a Windows service.

S3 Mirror Lite is a rewrite in C++ of [S3 Mirror](https://bitbucket.org/plantheric/s3-mirror) which was written in C#. 
S3 Mirror Lite was written in C++ to create a Windows service that has significantly lower resource usage. 

