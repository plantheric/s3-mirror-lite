#include "stdafx.h"
#include "MirrorConfig.h"

#include "utilities.h"
#include "Logging.h"

#include "ticpp.h"

using namespace ticpp;

MirrorConfig::MirrorConfig() : mMaxConcurrentUploads(2)
{
}

bool MirrorConfig::Load() {

	try {

		Document doc(application_directory() + "\\Mirror.Config");
		doc.LoadFile();

		Element* element = doc.FirstChildElement();
		element->GetAttribute("AccessKey", &mAccessKey);
		element->GetAttribute("SecretKey", &mSecretKey);
		element->GetAttribute("Bucket", &mBucket);
		element->GetAttribute("Region", &mRegion);
		element->GetAttribute("KeyPrefix", &mKeyPrefix);
		element->GetAttribute("MaxConcurrentUploads", &mMaxConcurrentUploads);

		Element* directories = element->FirstChildElement("Directories");
		Iterator<Element> directory("Directory");
		for (directory = directory.begin(directories); directory != directory.end(); directory++) {
			std::string filePath = directory->GetAttribute("Path");
			std::string keyPrefix = directory->GetAttribute("KeyPrefix");

			mDirectories.push_back(Directory{ filePath, keyPrefix });
		};
	}
	catch (Exception& e) {

		dlog.error << "Error to reading configuration - " << e.m_details << std::endl;
		return false;
	}
	return true;
}

MirrorConfig::~MirrorConfig()
{
}

std::string MirrorConfig::AccessKey() {

	return mAccessKey;
}

std::string MirrorConfig::SecretKey() {

	return mSecretKey;
}

std::string MirrorConfig::Bucket() {

	return mBucket;
}

std::string MirrorConfig::Region() {

	return mRegion;
}

std::string MirrorConfig::KeyPrefix() {

	return mKeyPrefix;
}

int MirrorConfig::MaxConcurrentUploads() {

	return mMaxConcurrentUploads;
}

const std::vector<MirrorConfig::Directory>& MirrorConfig::Directories() {

	return mDirectories;
}
