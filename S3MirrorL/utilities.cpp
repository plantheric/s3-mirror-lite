
#include "stdafx.h"
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <Shlwapi.h>

#include "utilities.h"
#include "Logging.h"

#include <vector>
#include <string>
#include <codecvt>

#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "Shlwapi.lib")

std::vector<byte> hmac(const std::vector<uint8_t>& key, const std::vector<uint8_t>& message)
{
	std::vector<byte> result;

	wrap<HCRYPTPROV, int> provider(NULL, [](HCRYPTPROV h){CryptReleaseContext(h, 0); });
	wrap<HCRYPTHASH, int> hash(NULL, [](HCRYPTHASH h){CryptDestroyHash(h); });
	wrap<HCRYPTKEY, int>  ckey(NULL, [](HCRYPTKEY h){CryptDestroyKey(h); });
	wrap<HCRYPTHASH, int> hmac(NULL, [](HCRYPTHASH h){CryptDestroyHash(h); });

	// Zero the HMAC_INFO structure and use the SHA1 algorithm for hashing.

	HMAC_INFO   hMacInfo;
	memset(&hMacInfo, 0, sizeof(hMacInfo));
	hMacInfo.HashAlgid = CALG_SHA1;

	//--------------------------------------------------------------------
	// Acquire a handle to the default RSA cryptographic service provider.

	if (!CryptAcquireContext(&provider, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
	{
		dlog.error << " Error in AcquireContext " << GetLastError();
		return result;
	}

	//--------------------------------------------------------------------
	// Derive a symmetric key from a hash object by performing the following steps:
	//    1. Call CryptCreateHash to retrieve a handle to a hash object.
	//    2. Call CryptHashData to add a text string (password) to the hash object.
	//    3. Import the key data provided by the callee.

	if (!CryptCreateHash(provider, CALG_SHA1, 0, 0, &hash))
	{
		dlog.error << "Error in CryptCreateHash " << GetLastError();
		return result;
	}

	if (!CryptHashData(hash, key.data(), key.size(), 0))
	{
		dlog.error << "Error in CryptHashData " << GetLastError();
		return result;
	}

	struct {
		BLOBHEADER header;
		DWORD length;
		BYTE key[1024];
	} keyBlob;

	keyBlob.header.bType = PLAINTEXTKEYBLOB;
	keyBlob.header.bVersion = CUR_BLOB_VERSION;
	keyBlob.header.reserved = 0;
	keyBlob.header.aiKeyAlg = CALG_RC2; // Used for import only
	keyBlob.length = key.size();

	memset(keyBlob.key, 0, sizeof(keyBlob.key));

	//	ASSERT(key.size() <= sizeof(key_blob.key));
	CopyMemory(keyBlob.key, key.data(), min(key.size(), sizeof(keyBlob.key)));

	if (!CryptImportKey(provider, (BYTE*)&keyBlob, sizeof(keyBlob), 0, CRYPT_IPSEC_HMAC_KEY, &ckey))
	{
		dlog.error << "Error in CryptImportKey " << GetLastError();
		return result;
	}

	//--------------------------------------------------------------------
	// Create an HMAC by performing the following steps:
	//    1. Call CryptCreateHash to create a hash object and retrieve a handle to it.
	//    2. Call CryptSetHashParam to set the instance of the HMAC_INFO structure into the hash object.
	//    3. Call CryptHashData to compute a hash of the message.
	//    4. Call CryptGetHashParam to retrieve the size, in bytes, of the hash.
	//    5. Resize result buffer to hold this.
	//    6. Call CryptGetHashParam again to retrieve the HMAC hash.

	if (!CryptCreateHash(provider, CALG_HMAC, ckey, 0, &hmac))
	{
		dlog.error << "Error in CryptCreateHash " << GetLastError();
		return result;
	}

	if (!CryptSetHashParam(hmac, HP_HMAC_INFO, (BYTE*)&hMacInfo, 0))
	{
		dlog.error << "Error in CryptSetHashParam " << GetLastError();
		return result;
	}

	if (!CryptHashData(hmac, message.data(), message.size(), 0))
	{
		dlog.error << "Error in CryptHashData " << GetLastError();
		return result;
	}

	DWORD hashLength = 0;

	if (!CryptGetHashParam(hmac, HP_HASHVAL, NULL, &hashLength, 0))
	{
		dlog.error << "Error in CryptGetHashParam " << GetLastError();
		return result;
	}

	result.resize(hashLength);

	if (!CryptGetHashParam(hmac, HP_HASHVAL, result.data(), &hashLength, 0))
	{
		dlog.error << "Error in CryptGetHashParam " << GetLastError();
		return result;
	}

	return result;
}

std::string base64(const std::vector<uint8_t>& data) {

	DWORD flags = CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF;
	DWORD stringLength = 0;
	std::vector<char> result;

	if (CryptBinaryToStringA(data.data(), data.size(), flags, NULL, &stringLength)) {
		result.resize(stringLength);
		CryptBinaryToStringA(data.data(), data.size(), flags, result.data(), &stringLength);
	}

	return to_string(result);
}

std::string remove_prefix(const std::string& text, const std::string& prefix) {

	std::string result;
	if (text.compare(0, prefix.size(), prefix) == 0) {
		result = text.substr(prefix.length());
	}
	return result;
}

std::string datetime_to_str() {

	std::time_t t = std::time(nullptr);
	std::tm tm;
	gmtime_s(&tm, &t);


	char buffer[26];
	asctime_s(buffer, sizeof(buffer), &tm);
	if (buffer[24] == '\n')
		buffer[24] = 0;

	return std::string(buffer);
}

std::time_t datetime_from_str(const std::string& dateTime) {

	std::tm tm;
	double sec = 0;
	sscanf_s(dateTime.c_str(), "%04d-%02d-%02dT%02d:%02d:%lfZ", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &sec);

	tm.tm_sec = static_cast<int>(sec);
	tm.tm_year -= 1900;
	tm.tm_isdst = -1;
	tm.tm_mon -= 1;

	return mktime(&tm);
}

static std::time_t FILETIME_to_time_t(FILETIME fileTime)
{
	FILETIME localFileTime;
	FileTimeToLocalFileTime(&fileTime, &localFileTime);
	SYSTEMTIME sysTime;
	FileTimeToSystemTime(&localFileTime, &sysTime);

	std::tm tm = { 0 };
	tm.tm_year = sysTime.wYear - 1900;
	tm.tm_mon = sysTime.wMonth - 1;
	tm.tm_mday = sysTime.wDay;
	tm.tm_hour = sysTime.wHour;
	tm.tm_min = sysTime.wMinute;
	tm.tm_sec = sysTime.wSecond;
	tm.tm_wday = 0;
	tm.tm_yday = 0;
	tm.tm_isdst = -1;
	time_t ret = mktime(&tm);
	return ret;
}

std::string application_directory() {

	TCHAR path[MAX_PATH];
	GetModuleFileName(NULL, path, MAX_PATH);
	PathRemoveFileSpec(path);

	std::string directory = utf16_to_utf8(path);

	return directory;
}

std::string ensure_suffix(std::string text, char suffix) {

	if (!text.empty() && text.back() != suffix) {
		text += suffix;
	}
	return text;
}

static std::string join_things(const std::string& first, const std::string& second, char condSep) {

	std::string res = ensure_suffix (first, condSep);
	res += second;
	return res;
}

std::string join_paths(const std::string& first, const std::string& second) {

	return join_things(first, second, '\\');
}

std::string join_keys(const std::string& first, const std::string& second) {

	return join_things(first, second, '/');
}

static bool should_list_file(WIN32_FIND_DATA& findData) {

	return (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0;
}

std::vector<std::pair<std::string, std::time_t>> files_in_directory(const std::string& path) {

	std::vector<std::pair<std::string, std::time_t>> files;
	WIN32_FIND_DATA findData;

	HANDLE findHandle = FindFirstFile(utf8_to_utf16(path).c_str(), &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
		return files;

	if (should_list_file(findData)) {
		files.emplace_back(utf16_to_utf8(findData.cFileName), FILETIME_to_time_t(findData.ftLastWriteTime));
	}

	while (FindNextFile(findHandle, &findData)) {
		if (should_list_file(findData)) {
			files.emplace_back(utf16_to_utf8(findData.cFileName), FILETIME_to_time_t(findData.ftLastWriteTime));
		}
	}
	FindClose(findHandle);
	return files;
}


std::string utf16_to_utf8(const std::wstring& utf16) {

	std::wstring_convert<std::codecvt_utf8_utf16 <wchar_t>, wchar_t> c;
	return c.to_bytes(utf16);
}

std::wstring utf8_to_utf16(const std::string& utf8) {

	std::wstring_convert<std::codecvt_utf8_utf16 <wchar_t>, wchar_t> c;
	return c.from_bytes(utf8);
}

std::string content_type_for_file(const std::string& filePath) {

	std::string contentType = "binary/octet-stream";

	auto buffer = to_buffer<std::string, wchar_t>(filePath);
	buffer.push_back(0);
	PTSTR ext = PathFindExtension(buffer.data());

	HKEY hKey;
	unsigned long bufferSize = buffer.size() * sizeof(buffer[0]);
	if (RegOpenKey(HKEY_CLASSES_ROOT, ext, &hKey) == ERROR_SUCCESS) {
		long q = RegQueryValueEx(hKey, _T("Content Type"), NULL, NULL, (byte*)buffer.data(), &bufferSize);
		if (q == ERROR_SUCCESS) {
			contentType = utf16_to_utf8(std::wstring(buffer.data()));
		}
	}
	return contentType;
}
