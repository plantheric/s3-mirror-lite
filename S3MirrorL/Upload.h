
#pragma once

#include <string>
#include <vector>
#include <thread>

#include "S3.h"

#include "coalescing_queue.h"

class Upload
{
public:

	Upload(const S3::Config& s3Config, int maxConcurrentUploads);
	~Upload();

	void Start();
	void Stop();

	struct Item {
		std::string filePath;
		std::string keyPrefix;
	};

	void TransferFile(const Item& item);
	void TransferFiles(const std::vector<Upload::Item>& items);

private:

	void PerformUpload();

	bool mRunning;
	coalescing_queue<Item> mPendingUploads;
	std::vector<std::thread> mUploadThreads;
	const S3 mS3;
	const int mMaxConcurrentUploads;
};

