
#pragma once

#include <string>
#include <vector>
#include <map>
#include <ctime>

class S3
{
public:
	struct Config {
		std::string mBucket;
		std::string mRegion;
		std::string mPath;
		std::string mAccessKey;
		std::string mSecret;
	};

	S3(const Config& config);
	~S3();

	bool Put(const std::string& localFilePath, const std::string& key) const;

	struct GetResponse {
		std::vector<uint8_t> data;
		bool valid;
	};
	GetResponse Get(const std::string& key) const;

	struct ListResponse {
		struct Object {
			std::string path;
			std::time_t modDate;
		};
		std::vector<Object> objects;
		bool valid;
	};
	ListResponse List(std::string key) const;

private:

	enum class Action : char;

	std::string MakeURL(Action action, const std::string& path = std::string()) const;
	std::string MakeHost() const;
	std::string GetEndpoint() const;

	std::string MakeParams(Action action, const std::string& path) const;
	std::map<std::string, std::string> MakeHeaders(Action action, const std::string& path = std::string()) const;
	std::string MakeSignature(Action action, const std::string& date, const std::string& path, const std::string& contentType) const;
	std::string VerbFromAction(Action action) const;

	const Config mConfig;
};

