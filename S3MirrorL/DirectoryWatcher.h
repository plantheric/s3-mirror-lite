
#pragma once

#include <string>
#include <thread>
#include <memory>
#include <functional>

class DirectoryWatcher
{
public:
	DirectoryWatcher(const std::string& path, std::function<void(const std::string&)> notify);
	~DirectoryWatcher();

	void Start();
	void Stop();

private:
	void Watch();

	std::string mPath;
	std::thread mThread;
	std::unique_ptr<OVERLAPPED> mOverlapped;
	bool mWatching;
	std::function<void (const std::string&)> mNotify;
};

