#include "stdafx.h"
#include "Request.h"

#include "Logging.h"
#include "utilities.h"

#include "curl/curl.h"

static std::string format_header(std::map<std::string, std::string>::value_type& h) {
	return std::string (h.first + ": " + h.second);
}

static long file_size(std::ifstream& file) {

	std::streampos current = file.tellg();

	file.seekg(0, std::ios::end);
	std::streampos length = file.tellg();

	file.seekg(current, std::ios::beg);

	return static_cast<long>(length);
}

Request::Request(const std::string& url, const std::map<std::string, std::string>& headers) :
					mURL(url), mHeaders(headers), mResponseCode(0)
{
}


Request::~Request()
{
}

bool Request::Send(const std::string& filePath) {

	mUploadFilePath = filePath;
	mFileStream.open(mUploadFilePath, std::ios::binary);
	if (mFileStream.is_open()) {
		return Send();
	}
	else {
		return false;
	}
}

bool Request::Send() {

	CURLcode res;

	CURL* curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, mURL.c_str());

		struct curl_slist* headers = NULL;

		for (auto& header : mHeaders) {
			headers = curl_slist_append(headers, format_header(header).c_str());
		}
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Request::ResponseCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);

		if (!mUploadFilePath.empty()) {
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
			curl_easy_setopt(curl, CURLOPT_INFILESIZE, file_size(mFileStream));
			curl_easy_setopt(curl, CURLOPT_READFUNCTION, Request::ReadCallback);
			curl_easy_setopt(curl, CURLOPT_READDATA, this);
		}

		res = curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &mResponseCode);

		if (mResponseCode != 200) {
			dlog.error << "Request returned " << mResponseCode << " for " << mURL << std::endl;
			dlog.error << "Response " << to_string(mResponse) << std::endl;
		}
		curl_easy_cleanup(curl);
		curl_slist_free_all(headers);
	}

	return mResponseCode == 200;
}

std::vector<uint8_t> Request::Response() {

	return mResponse;
}

size_t Request::ResponseCallback(void *contents, size_t size, size_t nmemb, void *user) {

	Request* request = static_cast<Request*>(user);
	size_t realSize = size * nmemb;

	uint8_t* buffer = static_cast<uint8_t*>(contents);
	request->mResponse.insert(request->mResponse.end(), buffer, &buffer[realSize]);

	return realSize;
}

size_t Request::ReadCallback(void *contents, size_t size, size_t nmemb, void *user) {

	Request* request = static_cast<Request*>(user);

	size_t realSize = size * nmemb;

	char* buffer = static_cast<char*>(contents);
	request->mFileStream.read(buffer, realSize);
	std::streamsize read = request->mFileStream.gcount();
	return static_cast<size_t>(read);
}

std::string Request::EncodeString(const std::string& text) {

	static CURL* curl = curl_easy_init();
	char* r = curl_easy_escape(curl, text.c_str(), text.length());
	std::string response = r;
	curl_free(r);
	return response;
}
