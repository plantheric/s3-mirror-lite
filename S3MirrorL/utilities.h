
#include <vector>
#include <string>
#include <tuple>
#include <functional>
#include <ctime>

template<typename C, typename T>
T join(C container, T del) {
	T result;

	for (auto& i : container) {
		if (!result.empty()) {
			result += del;
		}
		result += i;
	}
	return result;
}

std::vector<uint8_t> hmac(const std::vector<uint8_t>& key, const std::vector<uint8_t>& message);

template<typename C, typename B=uint8_t>
std::vector<B> to_buffer(C container) {

	return std::vector<B>(container.begin(), container.end());;
}

template<typename C>
std::string to_string(C container) {

	return std::string(container.begin(), container.end());
}

std::string remove_prefix(const std::string& text, const std::string& prefix);

std::string base64(const std::vector<uint8_t>& data);

template<typename T, typename I>
class wrap
{
public:
	wrap(I i, std::function<void(T)> c) : cleanup(c) {
		o = i;
	}
	~wrap() {
		cleanup(o);
	}
	T* operator&() { return &o; }
	operator T() { return o; }
private:
	wrap(wrap<T, I>& w);
	T o;
	std::function<void(T)> cleanup;
};

std::string datetime_to_str();
std::time_t datetime_from_str(const std::string& dateTime);

std::string application_directory();
std::string ensure_suffix(std::string text, char suffix);
std::string join_paths(const std::string& first, const std::string& second);
std::string join_keys(const std::string& first, const std::string& second);

std::vector<std::pair<std::string, std::time_t>> files_in_directory(const std::string& path);

std::string utf16_to_utf8(const std::wstring& utf16);
std::wstring utf8_to_utf16(const std::string& utf8);

std::string content_type_for_file(const std::string& filePath);

template<typename T>
T is_if(const T& value, bool condition) {
	return condition ? value : T();
}
