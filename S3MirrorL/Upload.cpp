#include "stdafx.h"
#include "Upload.h"

#include "Logging.h"

static bool operator<(const Upload::Item &l, const Upload::Item &r) {
	if (l.filePath == r.filePath)
		return l.keyPrefix < r.keyPrefix;
	else
		return l.filePath < r.filePath;
}

Upload::Upload(const S3::Config& s3Config, int maxConcurrentUploads) : 
				mRunning(false), mS3(s3Config), mMaxConcurrentUploads(maxConcurrentUploads)
{
}

Upload::~Upload()
{
	if (mRunning) {
		Stop();
	}
}

void Upload::TransferFiles(const std::vector<Upload::Item>& items) {

	for (auto& item : items) {
		TransferFile(item);
	}
}

void Upload::TransferFile(const Upload::Item& item) {

	dlog.info << "Scheduled " << item.filePath << std::endl;
	mPendingUploads.push(item);
}

void Upload::Start() {

	mRunning = true;
	for (int i = 0; i < mMaxConcurrentUploads; i++) {
		mUploadThreads.emplace_back(&Upload::PerformUpload, this);
	}
}

void Upload::Stop() {

	mRunning = false;
	mPendingUploads.close();
	for (auto& thread : mUploadThreads) {
		thread.join();
	}
}

void Upload::PerformUpload() {

	while (mRunning) {

		Item item;
		if (mPendingUploads.pop(item)) {

			dlog.info << "Starting " << item.filePath << std::endl;
			if (mS3.Put(item.filePath, item.keyPrefix)) {
				dlog.info << "Finished " << item.filePath << std::endl;
			}
			else {
				dlog.error << "Error uploading " << item.filePath << std::endl;
			}
		}
	}
}
