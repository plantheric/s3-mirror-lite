// S3MirrorLExe.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include "S3Mirror.h"



int _tmain(int argc, _TCHAR* argv[])
{
	S3Mirror mirror;
	mirror.Start();

	SetProcessWorkingSetSize(GetCurrentProcess(), -1, -1);

	while (getchar() != 'q');

	mirror.Stop();

	return 0;
}

