
#include "stdafx.h"
#include "DirectoryWatcher.h"
#include "utilities.h"

#include <locale>
#include <codecvt>
#include <vector>

DirectoryWatcher::DirectoryWatcher(const std::string &path,
									std::function<void(const std::string&)> notify) :
										mPath(path), mWatching(false), mNotify(notify)
{
}

DirectoryWatcher::~DirectoryWatcher()
{
	if (mWatching) {
		Stop();
	}
	mThread.join();
}

void DirectoryWatcher::Start()
{
	mWatching = true;
	mThread = std::thread(&DirectoryWatcher::Watch, this);
}

void DirectoryWatcher::Stop()
{
	mWatching = false;
	SetEvent(mOverlapped->hEvent);
}

void DirectoryWatcher::Watch()
{
	std::wstring_convert <std::codecvt_utf8_utf16 <wchar_t>, wchar_t> c;

	HANDLE directory = CreateFile(utf8_to_utf16(mPath).c_str(),
									FILE_LIST_DIRECTORY,
									FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
									NULL,
									OPEN_EXISTING,
									FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
									NULL);

	std::vector<byte> buffer(16 * 1024);
	mOverlapped = std::make_unique<OVERLAPPED>();
	mOverlapped->hEvent = CreateEvent(NULL, 0, 0, 0);

	while (true) {

		DWORD bytesReturned = 0;
		BOOL res = ReadDirectoryChangesW(directory, buffer.data(), buffer.size(),
							FALSE, FILE_NOTIFY_CHANGE_CREATION | FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME,
							&bytesReturned, mOverlapped.get(), NULL);

		res = GetOverlappedResult(directory, mOverlapped.get(), &bytesReturned, TRUE);

		if (!mWatching)
			break;

		FILE_NOTIFY_INFORMATION* info = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(buffer.data());
		bool processEntry = true;

		while (processEntry) {
			std::string path = utf16_to_utf8(std::wstring(info->FileName, info->FileNameLength / 2));
			mNotify(path);

			processEntry = info->NextEntryOffset != 0;
			info = (FILE_NOTIFY_INFORMATION*)((byte*)info + info->NextEntryOffset);
		}
	}

	CloseHandle(directory);
}
