#include "stdafx.h"
#include "S3Mirror.h"

#include <memory>

#include "DirectoryWatcher.h"
#include "UnMirroredFiles.h"
#include "S3.h"
#include "Upload.h"
#include "utilities.h"

#include "MirrorConfig.h"

S3Mirror::S3Mirror()
{
}

S3Mirror::~S3Mirror()
{
}

void S3Mirror::Start() {

	MirrorConfig mirrorConfig;
	if (!mirrorConfig.Load()) {
		return;
	}

	S3::Config config;
	config.mRegion = mirrorConfig.Region();
	config.mBucket = mirrorConfig.Bucket();
	config.mPath = mirrorConfig.KeyPrefix();
	config.mAccessKey = mirrorConfig.AccessKey();
	config.mSecret = mirrorConfig.SecretKey();

	mUploader = std::make_unique<Upload>(config, mirrorConfig.MaxConcurrentUploads());
	mUploader->Start();

	UnMirroredFiles unMirroredFiles(config);

	for (auto& directory : mirrorConfig.Directories()) {

		auto existingFiles = unMirroredFiles.GetFiles(directory.Path, directory.KeyPrefix);
		mUploader->TransferFiles(existingFiles);

		mWatchers.push_back(std::make_unique<DirectoryWatcher>(directory.Path, [this, directory](const std::string &fileName){
			mUploader->TransferFile(Upload::Item{ join_paths(directory.Path, fileName), join_keys(directory.KeyPrefix, fileName) });
		}));
	}

	for (auto& w: mWatchers) {
		w->Start();
	}
}

void S3Mirror::Stop() {

	for (auto& w : mWatchers) {
		w->Stop();
	}
	if (mUploader) {
		mUploader->Stop();
	}
}
