#include "stdafx.h"

#include "S3Mirror.h"

SERVICE_STATUS serviceStatus = { 0 };
SERVICE_STATUS_HANDLE serviceStatusHandle = NULL;
HANDLE serviceStopEvent = INVALID_HANDLE_VALUE;

VOID WINAPI ServiceMain(DWORD argc, LPTSTR* argv);
VOID WINAPI ServiceControlHandler(DWORD);

TCHAR ServiceName[] = _T("S3MirrorL");

int _tmain(int argc, TCHAR* argv[]) {

	int ret = 0;

	SERVICE_TABLE_ENTRY serviceTable[] = {
		{ ServiceName, ServiceMain },
		{ NULL, NULL }
	};

	if (StartServiceCtrlDispatcher(serviceTable) == FALSE) {
		ret = GetLastError();
	}

	return ret;
}

VOID WINAPI ServiceMain(DWORD argc, LPTSTR* argv) {

	serviceStatusHandle = RegisterServiceCtrlHandler(ServiceName, ServiceControlHandler);

	if (serviceStatusHandle == NULL) {
		return;
	}

	serviceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
	serviceStatus.dwControlsAccepted = 0;
	serviceStatus.dwCurrentState = SERVICE_START_PENDING;
	serviceStatus.dwWin32ExitCode = 0;
	serviceStatus.dwServiceSpecificExitCode = 0;
	serviceStatus.dwCheckPoint = 0;

	if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE) {
		return;
	}

	serviceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (serviceStopEvent == NULL) {

		serviceStatus.dwControlsAccepted = 0;
		serviceStatus.dwCurrentState = SERVICE_STOPPED;
		serviceStatus.dwWin32ExitCode = GetLastError();
		serviceStatus.dwCheckPoint = 1;
		SetServiceStatus(serviceStatusHandle, &serviceStatus);
		return;
	}

	serviceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
	serviceStatus.dwCurrentState = SERVICE_RUNNING;
	serviceStatus.dwWin32ExitCode = 0;
	serviceStatus.dwCheckPoint = 0;
	if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE) {
		return;
	}

	S3Mirror mirror;

	mirror.Start();

	WaitForSingleObject(serviceStopEvent, INFINITE);

	mirror.Stop();

	CloseHandle(serviceStopEvent);

	serviceStatus.dwControlsAccepted = 0;
	serviceStatus.dwCurrentState = SERVICE_STOPPED;
	serviceStatus.dwWin32ExitCode = 0;
	serviceStatus.dwCheckPoint = 0;
	if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE) {
		return;
	}
}

VOID WINAPI ServiceControlHandler(DWORD ctrlCode) {

	if (ctrlCode == SERVICE_CONTROL_STOP) {

		if (serviceStatus.dwCurrentState != SERVICE_RUNNING) {
			return;
		}

		serviceStatus.dwControlsAccepted = 0;
		serviceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		serviceStatus.dwWin32ExitCode = 0;
		serviceStatus.dwCheckPoint = 0;
		if (SetServiceStatus(serviceStatusHandle, &serviceStatus) == FALSE) {
			return;
		}

		SetEvent(serviceStopEvent);
	}
}

