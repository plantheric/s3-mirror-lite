#pragma once

#include <memory>
#include <vector>

class Upload;
class DirectoryWatcher;

class S3Mirror
{
public:
	S3Mirror();
	~S3Mirror();

	void Start();
	void Stop();

private:

	std::unique_ptr<Upload> mUploader;
	std::vector<std::unique_ptr<DirectoryWatcher>> mWatchers;
};

