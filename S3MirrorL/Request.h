#pragma once

#include <map>
#include <vector>
#include <fstream>

class Request {
public:
	Request(const std::string& url, const std::map<std::string, std::string>& headers);
	~Request();

	bool Send();
	bool Send(const std::string& filePath);

	std::vector<uint8_t> Response();
	long ResponseCode();

	static std::string EncodeString(const std::string&);

private:
	std::string mURL;
	std::map<std::string, std::string> mHeaders;

	static size_t ReadCallback(void *contents, size_t size, size_t nmemb, void *user);
	std::string mUploadFilePath;
	std::ifstream mFileStream;

	static size_t ResponseCallback(void *contents, size_t size, size_t nmemb, void *user);
	std::vector<uint8_t> mResponse;

	long mResponseCode;
};
