#pragma once

#include <map>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include "blocking_queue.h"

template<typename T>
class coalescing_queue {

public:

	coalescing_queue(const std::chrono::milliseconds& d = std::chrono::seconds(1)) : delay(d) {

		wait_thread = std::thread(&coalescing_queue<T>::process_wait_queue, this);
	}

	void push(const T& item) {

		std::unique_lock<std::mutex> lock(mutex);

		auto timeout = std::chrono::system_clock::now() + delay;
		auto it = wait_queue.emplace(item, timeout);

		if (it.second) {
			wait_queue_order.push_back(it.first);
		}
		else {
			it.first->second = timeout;
			std::sort(wait_queue_order.begin(), wait_queue_order.end(), [](const wait_queue_t::iterator& l, const wait_queue_t::iterator& r) {
				return l->second < r->second; });
		}
		conditional.notify_one();
	}

	bool pop(T& item) {

		return out_queue.pop(item);
	}

	void close() {

		out_queue.close();
		{
			std::unique_lock<std::mutex> lock(mutex);
			running = false;
			conditional.notify_all();
		}
		wait_thread.join();
	}

private:

	void process_wait_queue() {

		std::unique_lock<std::mutex> lock(mutex);
		while (running) {

			if (wait_queue_order.empty())
				conditional.wait(lock);
			else
				conditional.wait_until(lock, wait_queue_order.front()->second);

			while (running && !wait_queue_order.empty() && wait_queue_order.front()->second < std::chrono::system_clock::now()) {

				out_queue.push(wait_queue_order.front()->first);
				wait_queue.erase(wait_queue_order.front());
				wait_queue_order.pop_front();
			}
		}
	}

	bool running = true;

	typedef std::map<T, std::chrono::time_point<std::chrono::system_clock, std::chrono::system_clock::duration>> wait_queue_t;

	wait_queue_t wait_queue;
	std::deque<typename wait_queue_t::iterator> wait_queue_order;

	std::mutex mutex;
	std::thread wait_thread;
	std::condition_variable conditional;

	const std::chrono::milliseconds delay;
	blocking_queue<T> out_queue;
};
