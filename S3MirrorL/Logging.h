#pragma once

#include <iosfwd>

class Logging
{
public:
	Logging();
	~Logging();

	enum Level {Info, Warn, Error};

	std::ostream& info;
	std::ostream& warn;
	std::ostream& error;

	static Logging Log;
private:
	Level mLevel;
};

static Logging& dlog = Logging::Log;
