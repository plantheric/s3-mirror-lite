#pragma once

#include <vector>
#include <string>
#include "Upload.h"
#include "S3.h"

class UnMirroredFiles
{
public:
	UnMirroredFiles(const S3::Config& s3Config);
	~UnMirroredFiles();

	std::vector<Upload::Item> GetFiles(const std::string& directoryPath, const std::string& keyPrefix);

private:
	const S3 mS3;
};

