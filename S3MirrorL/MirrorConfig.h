
#pragma once

#include <vector>
#include <string>

class MirrorConfig
{
public:
	MirrorConfig();
	~MirrorConfig();

	bool Load();


	std::string AccessKey();
	std::string SecretKey();
	std::string Bucket();
	std::string Region();
	std::string KeyPrefix();
	int			MaxConcurrentUploads();

	struct Directory {
		const std::string Path;
		const std::string KeyPrefix;
	};
	const std::vector<Directory>& Directories();

private:
	std::string mAccessKey;
	std::string mSecretKey;
	std::string mBucket;
	std::string mRegion;
	std::string mKeyPrefix;
	int mMaxConcurrentUploads;

	std::vector<Directory> mDirectories;
};

