#include "stdafx.h"
#include "UnMirroredFiles.h"

#include <future>

#include "utilities.h"

UnMirroredFiles::UnMirroredFiles(const S3::Config& s3Config) : mS3(s3Config)
{
}


UnMirroredFiles::~UnMirroredFiles()
{
}

std::vector<Upload::Item> UnMirroredFiles::GetFiles(const std::string& directoryPath, const std::string& keyPrefix) {

	std::vector<Upload::Item> uploadFiles;

	std::future<S3::ListResponse> remotes = std::async([this, &keyPrefix](){
		auto response = mS3.List(keyPrefix);
		std::sort(response.objects.begin(), response.objects.end(), [](S3::ListResponse::Object& l, S3::ListResponse::Object& r) -> bool {return l.path < r.path; });
		return response;
	});

	auto localFiles = files_in_directory(join_paths(directoryPath, "*"));
	std::sort(localFiles.begin(), localFiles.end(), [](std::pair<std::string, std::time_t>& l, std::pair<std::string, std::time_t>& r) -> bool {return l.first < r.first; });

	auto remoteFiles = remotes.get();

	if (remoteFiles.valid) {

		auto remoteBegin = remoteFiles.objects.begin();
		for (auto& local : localFiles) {

			auto remote = std::find_if(remoteBegin, remoteFiles.objects.end(), [&local](S3::ListResponse::Object& r){return r.path == local.first; });

			if (remote == remoteFiles.objects.end() || remote->modDate < local.second) {
				uploadFiles.push_back(Upload::Item{ join_paths(directoryPath, local.first), join_keys(keyPrefix, local.first) });
			}
			if (remote != remoteFiles.objects.end()) {
				remoteBegin = next(remote);
			}
		}
	}

	return uploadFiles;
}
