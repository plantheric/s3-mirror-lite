#pragma once

#include <queue>
#include <mutex>
#include <condition_variable>

template<typename T>
class blocking_queue {
public:

	void push(const T& item) {

		std::unique_lock<std::mutex> lock(mutex);
		if (running) {
			queue.push(item);
			conditional.notify_one();
		}
	}

	bool pop(T& item) {

		std::unique_lock<std::mutex> lock(mutex);
		while (running && queue.empty()) {
			conditional.wait(lock);
		}

		if (running) {
			item = queue.front();
			queue.pop();
		}
		return running;
	}

	void close() {

		std::unique_lock<std::mutex> lock(mutex);
		running = false;
		conditional.notify_all();
	}

private:
	bool running = true;
	std::queue<T> queue;
	std::mutex mutex;
	std::condition_variable conditional;
};