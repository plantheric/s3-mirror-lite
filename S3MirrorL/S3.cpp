#include "stdafx.h"
#include "S3.h"

#include "Request.h"
#include "Logging.h"

#include "utilities.h"
#include "ticpp.h"

using namespace ticpp;

#include <map>

enum class S3::Action : char {
	List,
	Get,
	Put
};


S3::S3(const Config& config) :
		mConfig(config)
{
}

S3::~S3()
{
}

bool S3::Put(const std::string& localFilePath, const std::string& key) const {

	auto action = Action::Put;
	std::string keyPath = join_keys(Request::EncodeString(mConfig.mPath), Request::EncodeString(key));
	Request request(MakeURL(action, keyPath), MakeHeaders(action, keyPath));
	return request.Send(localFilePath);
}

S3::GetResponse S3::Get(const std::string& key) const {

	auto action = Action::Get;
	std::string keyPath = join_keys(Request::EncodeString(mConfig.mPath), Request::EncodeString(key));
	Request request(MakeURL(action, keyPath), MakeHeaders(action, keyPath));

	GetResponse response;
	response.valid = request.Send();
	response.data = request.Response();
	return response;
}

S3::ListResponse S3::List(std::string key) const {

	auto action = Action::List;
	key = ensure_suffix(key, '/');

	Request request(MakeURL(action, key), MakeHeaders(action));

	ListResponse response;
	response.valid = request.Send();

	try {
		Document doc;
		doc.Parse(to_string(request.Response()));

		std::string prefix = join_keys(mConfig.mPath, key);

		Element* element = doc.FirstChildElement();
		Iterator<Element> content("Contents");
		for (content = content.begin(element); content != content.end(); content++) {
			Element* key = content->FirstChildElement("Key");
			Element* mod = content->FirstChildElement("LastModified");

			response.objects.push_back(ListResponse::Object{ remove_prefix(key->GetText(), prefix), datetime_from_str(mod->GetText()) });
		};
	}
	catch (Exception& e) {

		dlog.error << "Error to reading S3 List response - " << e.m_details << std::endl;
	}

	return response;
}

std::string S3::MakeURL(Action action, const std::string& path) const {

	std::string urlPath = is_if(path, action != Action::List);

	return "https://" + MakeHost() + "/" + urlPath + MakeParams(action, path);
}

std::string S3::MakeHost() const {

	return mConfig.mBucket + "." + GetEndpoint();
}

std::string S3::GetEndpoint() const {

	std::map<std::string, std::string> endPoints = {
		{ "us-east-1", "s3.amazonaws.com" },
		{ "us-west-2", "s3-us-west-2.amazonaws.com" },
		{ "us-west-1", "s3-us-west-1.amazonaws.com" },
		{ "eu-west-1", "s3-eu-west-1.amazonaws.com" },
		{ "eu-central-1", "s3-eu-central-1.amazonaws.com" },
		{ "ap-southeast-1", "s3-ap-southeast-1.amazonaws.com" },
		{ "ap-southeast-2", "s3-ap-southeast-2.amazonaws.com" },
		{ "ap-northeast-1", "s3-ap-northeast-1.amazonaws.com" },
		{ "sa-east-1", "s3-sa-east-1.amazonaws.com" } };

	return endPoints[mConfig.mRegion];
}

std::string S3::MakeParams(Action action, const std::string& path) const {

	return is_if("?prefix=" + join_keys(mConfig.mPath, path), action == Action::List);
}

std::map<std::string, std::string> S3::MakeHeaders(Action action, const std::string& path) const {

	std::map<std::string, std::string> headers;
	std::string date = datetime_to_str();
	std::string contentType;

	if (!path.empty()) {
		contentType = content_type_for_file(path);
		headers["Content-Type"] = contentType;
	}

	headers["Date"] = date;
	headers["Host"] = MakeHost();
	headers["Authorization"] = "AWS " + mConfig.mAccessKey + ":" + MakeSignature(action, date, path, contentType);

	return headers;
}

std::string S3::MakeSignature(Action action, const std::string& date, const std::string& path, const std::string& contentType) const {

	std::string verb = VerbFromAction(action);
	std::string contentMD5;
	std::string canonicalizedAmzHeaders;
	std::string canonicalizedResource = "/" + mConfig.mBucket + "/" + path;

	std::string canon = join(std::vector<std::string> { verb, contentMD5, contentType, date, canonicalizedAmzHeaders + canonicalizedResource }, std::string("\n"));

	std::string signature = base64(hmac(to_buffer(mConfig.mSecret), to_buffer(canon)));
	return signature;
}

std::string S3::VerbFromAction(Action action) const {

	switch (action) {
	case Action::List:
	case Action::Get:
	default:
		return "GET";
	case Action::Put:
		return "PUT";
	}
}