# S3 Mirror Lite

S3 Mirror Lite is a tool for Windows that allows you to mirror local directories to Amazon S3.
S3 Mirror Lite can be run as either a command line application or installed as a Windows service.

S3 Mirror Lite is a rewrite in C++ of [S3 Mirror](https://bitbucket.org/plantheric/s3-mirror) which was written in C#. 
S3 Mirror Lite was written in C++ to create a Windows service that has significantly lower resource usage. 

## Configuration
S3 Mirror Lite is configured by editing the `Mirror.config` file.

	<MirrorConfig AccessKey="" SecretKey="" Bucket="" Region="" KeyPrefix="" MaxConcurrentUploads="2">
	  <Directories>
		<Directory Path="" KeyPrefix="" />
		<Directory Path="" KeyPrefix="" />
	  </Directories>
	</MirrorConfig>

### AccessKey, SecretKey
For security reasons you should generate an `AccessKey` and `SecretKey` specifically for S3 Mirror Lite to use. 
The credentials should be configured to only allow read and write access to the Bucket you want to mirror to. 

### Bucket, Region
`Bucket` should just be the name of the bucket you want to upload to. 
`Region` should be the lower case string version of the region name e.g. 'eu-west-1' as listed in 
[Regions and Endpoints - Amazon Web Services](http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region)

### KeyPrefix
`KeyPrefix` can be specified for the whole config and for each directory. 
This allows you to build S3 keys to create a folder hierarchy for your files.

### MaxConcurrentUploads
The maximum number of uploads the software will perform concurrently. This can be altered to balance performance and resource use.

## Running

On launch the software will scan the local directories and compare their contents with the S3 bucket. 
Any files where the remote file is either missing or out of date will be queue for upload.

The software will then watch the specified directories for new or modified files. The files will be immediately queued for upload.

### S3Mirror.exe

`S3Mirror.exe` can be run from a `cmd.exe` prompt or by double clicking the .exe. It can be used when you want to manually trigger mirroring. 
It can also be used to easily see the program output whilst initially configuring the software.

### S3MirrorService.exe

Comming soon.
